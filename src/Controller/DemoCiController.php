<?php

namespace App\Controller;

use App\Entity\DemoCi;
use App\Form\DemoCiType;
use App\Repository\DemoCiRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/demo')]
class DemoCiController extends AbstractController
{
    #[Route('/', name: 'app_demo_ci_index', methods: ['GET'])]
    public function index(DemoCiRepository $demoCiRepository): Response
    {
        return $this->render('demo_ci/index.html.twig', [
            'demo_cis' => $demoCiRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_demo_ci_new', methods: ['GET', 'POST'])]
    public function new(Request $request, DemoCiRepository $demoCiRepository): Response
    {
        $demoCi = new DemoCi();
        $form = $this->createForm(DemoCiType::class, $demoCi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $demoCiRepository->add($demoCi, true);

            return $this->redirectToRoute('app_demo_ci_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('demo_ci/new.html.twig', [
            'demo_ci' => $demoCi,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_demo_ci_show', methods: ['GET'])]
    public function show(DemoCi $demoCi): Response
    {
        return $this->render('demo_ci/show.html.twig', [
            'demo_ci' => $demoCi,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_demo_ci_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, DemoCi $demoCi, DemoCiRepository $demoCiRepository): Response
    {
        $form = $this->createForm(DemoCiType::class, $demoCi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $demoCiRepository->add($demoCi, true);

            return $this->redirectToRoute('app_demo_ci_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('demo_ci/edit.html.twig', [
            'demo_ci' => $demoCi,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_demo_ci_delete', methods: ['POST'])]
    public function delete(Request $request, DemoCi $demoCi, DemoCiRepository $demoCiRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $demoCi->getId(), $request->request->get('_token'))) {
            $demoCiRepository->remove($demoCi, true);
        }

        return $this->redirectToRoute('app_demo_ci_index', [], Response::HTTP_SEE_OTHER);
    }
}
