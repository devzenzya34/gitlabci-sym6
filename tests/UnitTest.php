<?php

namespace App\Tests;

use App\Entity\DemoCi;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testDemoCi(): void
    {
        $demo = new DemoCi();
        $demo->setDemo('test');

        $this->assertTrue($demo->getDemo() === 'test');
    }
}
